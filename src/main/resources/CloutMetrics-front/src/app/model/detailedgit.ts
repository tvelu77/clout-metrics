export class DetailedGit {
  gitId!: string;
  gitName!: string;
  addedDate!: string;
  languagesRatio!: string;
  numberCommits!: string;
}
